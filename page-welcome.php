<?php get_template_part('template-parts/content-front-page') ?>

<div class="container bg-gray-50 p-20">
    <div class="">
        <p class="headline-sm m-b-20">WooCommerce Files</p>
        <p class="headline-xs m-b-20 c-gray-600">location & usage</p>
        <div class="">
            <div>
                <p class="p-5"><i class="fas fa-folder c-gray-500"></i> woocommerce/</p>
                <p class="m-l-20"><i class="fa fa-folder c-gray-500"></i> auth/</p>  
                <div class="p-tb-5 m-l-30">
                    <p class="p-5">footer.php</p>   
                    <p class="p-5">form-grant-access.php</p>   
                    <p class="p-5">form-login.php</p>   
                    <p class="p-5">header.php</p>                 
                </div>         
                <p class="m-l-20"><i class="fa fa-folder c-gray-500"></i> cart/</p>    
                <div class="p-tb-5 m-l-30">
                    <p class="p-5">woocommerce/cart/cart-empty.php</p>               
                    <p class="p-5">woocommerce/cart/cart-item-data.php</p>               
                    <p class="p-5">woocommerce/cart/cart-shipping.php</p>               
                    <p class="p-5">woocommerce/cart/cart-totals.php</p>               
                    <p class="p-5">woocommerce/cart/cart-cart.php</p>               
                    <p class="p-5">woocommerce/cart/cross-sells.php</p>               
                    <p class="p-5">woocommerce/cart/mini-cart.php</p>               
                    <p class="p-5">woocommerce/cart/proceed-to-checkout.php</p>               
                    <p class="p-5">woocommerce/cart/shipping-calculator.php</p>               
                </div>         
                <p class="m-l-20"><i class="fa fa-folder c-gray-500"></i> checkout/</p>
                <div class="p-tb-5 m-l-30">
                    <p class="p-5">woocommerce/checkout/cart-errors.php</p>             
                    <p class="p-5">woocommerce/checkout/form-billing.php</p>             
                    <p class="p-5">woocommerce/checkout/form-checkout.php</p>             
                    <p class="p-5">woocommerce/checkout/form-coupon.php</p>             
                    <p class="p-5">woocommerce/checkout/form-login.php</p>             
                    <p class="p-5">woocommerce/checkout/form-pay.php</p>             
                    <p class="p-5">woocommerce/checkout/form-shipping.php</p>             
                    <p class="p-5">woocommerce/checkout/order-receipt.php</p>             
                    <p class="p-5">woocommerce/checkout/payment-receipt.php</p>             
                    <p class="p-5">woocommerce/checkout/payment.php</p>             
                    <p class="p-5">woocommerce/checkout/review-order.php</p>             
                    <p class="p-5">woocommerce/checkout/terms.php</p>             
                    <p class="p-5">woocommerce/checkout/thankyou.php</p>             
                </div>
                <p class="m-l-20 p-b-5"><i class="fa fa-folder c-gray-500"></i> emails/</p>
                <p class="m-l-30"><i class="fa fa-folder c-gray-500"></i> carplain/</p>          
                <div class="p-tb-5 m-l-40">
                    <p class="p-5">woocommerce/emails/plain/</p>           
                    <p class="p-5">woocommerce/emails/plain/admin-cancelled-orders.php</p>           
                    <p class="p-5">woocommerce/emails/plain/admin-failed-order.php</p>           
                    <p class="p-5">woocommerce/emails/plain/admin-new-order.php</p>           
                    <p class="p-5">woocommerce/emails/plain/customer-completed-order.php</p>           
                    <p class="p-5">woocommerce/emails/plain/customer-invoice.php</p>           
                    <p class="p-5">woocommerce/emails/plain/customer-new-account.php</p>           
                    <p class="p-5">woocommerce/emails/plain/customer-note.php</p>           
                    <p class="p-5">woocommerce/emails/plain/customer-on-hold-order.php</p>           
                    <p class="p-5">woocommerce/emails/plain/customer-processing-order.php</p>           
                    <p class="p-5">woocommerce/emails/plain/customer-refunded-order.php</p>           
                    <p class="p-5">woocommerce/emails/plain/customer-reset-password.php</p>                            
                </div>  
                
                <p class="p-5">woocommerce/archive-product.php</p>           
                <p class="p-5">woocommerce/content-product.php</p>           
                <p class="p-5">woocommerce/content-product_cat.php</p>           
                <p class="p-5">woocommerce/content-single-product.php</p>           
                <p class="p-5">woocommerce/content-widget-product.php</p>           
                <p class="p-5">woocommerce/widget-reviews.php</p>           
                <p class="p-5">woocommerce/product-searchform.php</p>           
                <p class="p-5">woocommerce/sinle-product-reviews.php</p>           
                <p class="p-5">woocommerce/single-product.php</p>           
                <p class="p-5">woocommerce/taxonomy-product_cat.php</p>           
                <p class="p-5">woocommerce/taxonomy-product-tag.php</p>                         
            </div>
        </div>
    </div>
</div>