import ImagesLoaded from 'imagesloaded';
import Shuffle from 'shufflejs';

class IsotopeGrid {

    constructor(itemListwrapper, itemContainer, filterTrigger) {

        this.itemListwrapper = itemListwrapper;
        this.itemContainer = itemContainer;
        this.filterTrigger = filterTrigger;

        this.resetButton = $('.button--reset');

        this.isotopeGridInit(this.itemListwrapper, this.itemContainer, this.filterTrigger);
        this.events();
    }


    events() {

    }

    filterItems() {
        shuffleInstance.filter('space');
    }
    resetFilterItems() {

    }

    isotopeGridInit(itemListwrapper, itemContainer, filterTrigger) {

        let images = new ImagesLoaded('body', {
            background: '.load-item'
        }, () => {

            //const Shuffle = window.Shuffle;

            const element = document.querySelector(itemListwrapper);

            if ($(element).length > 0) {

                const sizer = element.querySelector(itemContainer);

                let shuffleInstance = new Shuffle(element, {
                    itemSelector: filterTrigger,
                    sizer: sizer,
                    speed: 600
                });

                // get filter buttons 
                $(".button-group button").each((key, value) => {

                    $(value).on("click", (e) => {

                        let button = $(e.target);

                        shuffleInstance.filter($(button).data("group"));

                        $(".button-group button").removeClass("active-button");
                        $(button).addClass("active-button");
                    });
                });
            }
        });
    }
}

export default IsotopeGrid;
