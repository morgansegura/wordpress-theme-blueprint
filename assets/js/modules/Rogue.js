import AOS from "aos";
import imagesloaded from "imagesloaded";

class Rogue {
    constructor() {
        this.head = $("head");
        this.body = $("body");
        this.loading = $(".load-item");
        this.loader = $(".loader");
        this.loaderCounter = $(".loader__counter");
        AOS.init();
        this.events();
        this.loaderIsVisible = false;
    }
    events() {
        this.approvedOutlines();
        this.loadBlock();
    }
    approvedOutlines() {
        // ADA approved remove outlines
        this.body.on("mousedown", () => {
            this.head.find("#outlineADA").html("*{outline:none}")
        });
        this.body.on("keydown", () => {
            this.head.find("#outlineADA").html("")
        });
    }
    loadBlock() {
        
        if ( this.loading.length > 0 ) {

            let loaded = new imagesloaded(this.loading);

            loaded.on('always', (instance) => {
                this.loader.addClass('loading--is-hidden');
                console.log('content loaded');
            });

            loaded.on('done', (instance) => {

                // console.log('all images successfully loaded');
            })
            loaded.on('fail', () => {
                // console.log('all images loaded, at least one is broken');
            })
            loaded.on('progress', (instance, image) => {
                let result = image.isLoaded ? 'loaded' : 'broken';
                console.log('image is ' + result + ' for ' + image.img.src);
            })
        }
    }
    // Edit this counter *
    loadTimeCounter() { 
        let n = localStorage.getItem(this.loaderCounter);

        if (n === null) {
            n = 0;
        }
        n++;

        localStorage.setItem(this.loaderCounter, n);
        this.loaderCounter.html(n);
    }
}
export default Rogue;