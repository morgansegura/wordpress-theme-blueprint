class Search {
    // 1. describe and creat/initiate our object
    constructor() {
        this.addModalHTML();
        this.resultsDiv = $("#search-overlay__results");
        this.openButton = $(".trigger__search");
        this.closeButton = $(".search .trigger__close");
        this.searchField = $("#search-term");
        this.overlay = $(".search.overlay__light");
        this.events();
        this.isOverlayOpen = false;
        this.isSpinnerVisible = false;
        this.previousValue;
        this.typingTimer;
    }
    // 2. events
    events() {
        this.openButton.on("click", this.openOverlay.bind(this));
        this.closeButton.on("click", this.closeOverlay.bind(this));
        // this.searchField.on("focus", this.togglePlaceholder.bind(this));
        $(document).on("keydown", this.keyPressDispatcher.bind(this));
        this.searchField.on("keyup", this.typingLogic.bind(this));
        this.typingTimer;
        this.togglePlaceholder();
    }
    openOverlay(e) {
        e.preventDefault();
        this.overlay.addClass("overlay--is-visible");
        $("body").addClass("no-scroll");
        this.searchField.val('');
        setTimeout(() => this.searchField.focus(), 301);            
        this.isOverlayOpen = true;

        return false;
    }
    closeOverlay() {
        this.overlay.removeClass("overlay--is-visible");
        $("body").removeClass("no-scroll");
        this.isOverlayOpen = false;
    }

    keyPressDispatcher(e) {
        // if (e.keyCode == 83 && !this.isOverlayOpen && !$("input, textarea").is(":focus")) {
        //     this.openOverlay();
        // }

        if (e.keyCode == 27 && this.isOverlayOpen) {
            this.closeOverlay();
        }
    }

    togglePlaceholder() {
        // onfucusin hide placeholder
        let placeholder = this.searchField.attr("placeholder");
        this.searchField.on("focus", () => {
            this.searchField.addClass('is--focussed');
        });
        this.searchField.on("blur", () => {
            this.searchField.removeClass('is--focussed');
        });
    }

    typingLogic() {
        if (this.searchField.val() != this.previousValue) {
            clearTimeout(this.typingTimer);

            if (this.searchField.val()) {
                if (!this.isSpinnerVisible) {
                    this.resultsDiv.html('<div class="spinner-loader"></div>');
                    this.isSpinnerVisible = true;
                }
                this.typingTimer = setTimeout(this.getResults.bind(this), 750);
            } else {
                this.resultsDiv.html('<h2 class="secondaryFont bold-6 t-center headline-sm p-t-50">All Done?</h2>');
                this.isSpinnerVisible = false;
            }
        }
        this.previousValue = this.searchField.val();
    }
    getResults() {
        $.getJSON(themeData.root_url + '/wp-json/theme/v1/search?term=' + this.searchField.val(), (results) => {
            console.log(results);
            this.resultsDiv.html(`
                <div class="search__results-div container flex wrap gaps-20 p-20"
                    data-aos="fade"
                    data-aos-offset="200"
                    data-aos-delay="50"
                    data-aos-duration="1000"
                    data-aos-easing="ease-in-out"
                    data-aos-mirror="true"
                    data-aos-once="false"
                    data-aos-anchor-placement="top-center">
                    <div class="general-results box-12">
                        <h2>General Information</h2>
                        ${results.generalInfo.length ? '<ul class="link-list min-list">' : '<p>No general information matches that search.</p>'}                
                        ${results.generalInfo.map(item => `
                        <li>
                        <a href="${item.permalink}">${item.title}</a> 
                        <p>${item.excerpt}</p>
                        </li>`).join('')}                    
                        ${results.generalInfo.length ? '</ul>' : ''} 
                    </div>
                    <div class="box-md-4">
                        <h2>Programs</h2>
                        ${results.programs.length ? '<ul class="link-list min-list">' : `<p>No programs match that search. <a class="btn" href="${themeData.root_url}/programs">View all programs</a></p>`}                
                        ${results.programs.map(item => `<li><a href="${item.permalink}">${item.title}</a></li>`).join('')}                    
                        ${results.programs.length ? '</ul>' : ''} 

                        <h2>Professors</h2>
                        ${results.professors.length ? '<ul class="professor-cards">' : `<p>No professors match that search.</p>`}                
                        ${results.professors.map(item => `
                        <li class="professor-card__list-item">
                            <a class="professor-card" href="${item.permalink}">
                            <img class="professor-card__image" src="${item.image}">
                            <span class="professor-card__name">${item.title}</span>
                            </a>
                        </li>
                        `).join('')}                    
                        ${results.professors.length ? '</ul>' : ''} 
                    </div>
                    <div class="box-md-4">
                        <h2>Campuses</h2>
                        ${results.campuses.length ? '<ul class="link-list min-list">' : `<p>No campuses match that search. <a class="btn" href="${themeData.root_url}/campuses">View all campuses</a></p>`}                
                        ${results.campuses.map(item => `<li><a href="${item.permalink}">${item.title}</a> </li>`).join('')}                    
                        ${results.campuses.length ? '</ul>' : ''}  

                        <h2>Events</h2>
                        ${results.events.length ? '' : `<p>No events match that search. <a class="btn" href="${themeData.root_url}/events">View all events</a></p>`}                
                        ${results.events.map(item => `
                        <div class="event-summary">
                            <a class="event-summary__date t-center" href="${item.permalink}">
                                <span class="event-summary__month">${item.month}</span>
                                <span class="event-summary__day">${item.day}</span>  
                            </a>
                            <div class="event-summary__content">
                                <h5 class="event-summary__title headline headline--tiny"><a class="btn" href="${item.permalink}">${item.title}</a></h5>
                                <p>
                                    ${item.description}
                                    <a href="${item.permalink}" class="nu gray">Learn more</a>
                                </p>
                            </div>
                        </div>
                        `).join('')}                    
                    </div>
                </div>
            `);
            this.isSpinnerVisible = false;
        });
    }

    addModalHTML() {
        $("body").append(`
            <div class="search overlay overlay__light">
                <div class="trigger__close"><i class="fas fa-times-circle"></i></div>
                <div class="container">
                    <div class="search overlay__top bg-gray-300">
                        <div class="flex justify-center items-center"
                            data-aos="flip-down"
                            data-aos-offset="200"
                            data-aos-delay="50"
                            data-aos-duration="1000"
                            data-aos-easing="ease-in-out"
                            data-aos-mirror="true"
                            data-aos-once="false"
                            data-aos-anchor-placement="top-center">
                            <input type="text" class="search__term box-8" placeholder="Search Away!" id="search-term">
                            <div class="search__icon" title="Reset the search field."><i class="fas fa-sync"></i></div>
                        </div>
                    </div>
                    <div class="search__results">
                        <div id="search-overlay__results" class="search__results__content">
                            <h2 class="secondaryFont bold-6 t-center headline-sm p-tb-50">Search Away!</h2>
                        </div>
                    </div>
                </div>
            </div>
        `);
    }
}

export default Search;