class ContactForm {
    constructor() {
        this.openButton = $(".trigger__contact");
        this.events();
        this.isOverlayOpen = false;
        this.isSpinnerVisible = false;
    }

    events() {
        this.openButton.on("click", this.openOverlay.bind(this));
        $(document).on("keydown", this.keyPressDispatcher.bind(this));  
    }
    openOverlay() {
        this.addModalHTML();
        const overlay = $(".contact.overlay__dark");

        if ( overlay !== undefined ) {
            $(overlay).addClass("overlay--is-visible");
            $("body").addClass("no-scroll");
            this.isOverlayOpen = true;
            this.parseForm();
            $(".contact .trigger__close").on("click", () => {
                this.closeOverlay()
            });
        }
        return false;
    }
    closeOverlay() {
        const overlay = $(".contact.overlay__dark");
        $(overlay).removeClass("overlay--is-visible");
        $("body").removeClass("no-scroll");
        $(overlay).replaceWith("");
        this.isOverlayOpen = false;
    }

    keyPressDispatcher(e) {
        if (e.keyCode == 27 && this.isOverlayOpen) {
            this.closeOverlay();
        }
    }
 
    checkInvalid() {
        $('#contact__form > input, #contact__form form textarea').on('keyup', () => {
            $(this).addClass('valid');
        });
    }
    insertHTML() {
        return (`
            <div id="contact__form">
                <form class="light-style" action="${themeData.theme_url}/inc/contact-form.php" role="form" method="post" id="contactForm">
                    <h3 class="bold-6 t-size-xl t-center p-tb-10">We are excited to hear from you.</h3>
                    <p class="t-center c-gray-700 p-b-20">Please leave a descript message and your contact information below.</p>
                    <div class="grid gap-10">
                        <div class="box-sm-6">
                            <input id="contact-name" type="text" name="name" placeholder="Name" required>
                        </div>
                        <div class="box-sm-6">
                            <input id="contact-email" type="email" name="email" placeholder="Email" required>
                        </div>
                    </div>
                    
                    <input id="contact-subject" type="text" name="subject" placeholder="Subject" required>
                    
                    <textarea id="contact-message" name="message" placeholder="Message" required></textarea>
                    
                    <div class="submit inline-block">
                        <input class="bg-red-500 c-red" id="submit" value="Send Message" type="submit">
                    </div>
                </form>
            </div>
        `);
        console.log("Form HTML is in the modal");
        console.log(`+-------+`);
    }

    addModalHTML() {
        $("body").append(`
            <div class="contact overlay overlay__dark">
                <div class="trigger__close">
                    <i class="fas fa-times-circle"></i>
                </div>
                <div class="container">
                    <div class="overlay__top message bg-gray-800 c-white m-b-50">
                        <div id="formMessage" class="flex justify-center items-center">
                            <h3 class="bold-6">Please update the form below with your contact information.</h3>
                        </div>
                    </div>
                    <div class="modal bg-white"
                        data-aos="fade-up"
                        data-aos-offset="200"
                        data-aos-delay="50"
                        data-aos-duration="1000"
                        data-aos-easing="ease-in-out"
                        data-aos-mirror="true"
                        data-aos-once="false"
                        data-aos-anchor-placement="top-center"
                         ${this.insertHTML()}
                    </div>
                </div>
            </div>
        `);
    }

    parseForm() {
        
        const contactForm = $("#contactForm");

        $(contactForm).submit((e) => {
            e.preventDefault(); //prevent default action 
            let proceed = true;
            const targetForm = $(e.target);
            const requiredField = $(e.target).find(":required");

            // User-side validation
            $(requiredField).each((index, value) => {

                let item = $(value)[0];

                $(item).addClass('valid').removeClass('invalid');

                if (!$.trim($(item).val())) {
                    $(item).addClass('invalid').removeClass('valid');
                    proceed = false;
                }

                let email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if ($(item).attr("type") == "email" && !email_reg.test($.trim($(item).val()))) {
                    $(item).addClass('invalid').removeClass('valid');
                    proceed = false;      
                }

            }).keyup(function () { //reset previously set border colors on .keyup()
                $(item).addClass('valid').removeClass('invalid');
            }).change(function () {
                $(item).addClass('valid').removeClass('invalid');
            });

            // Ready to submit
            if (proceed) {
                const postURL = themeData.mail_url;
                const requestMethod = 'POST';
                const formData = $(targetForm).serialize();

                $.ajax({
                    url: postURL,
                    type: requestMethod,
                    dataType: 'json',
                    data: formData,
                }).done((response) => {
                    $("#formMessage").html(`
                        <h3 class="bold-6">${response.text}</h3>
                    `);
                    $(".overlay__top").addClass("success");
                    $(".overlay .trigger__close").addClass("success");
                    $(contactForm).html(`
                    <div class="success__check"                            
                        data-aos="zoom-in-up"
                        data-aos-offset="200"
                        data-aos-delay="50"
                        data-aos-duration="500"
                        data-aos-easing="ease-in-out"
                        data-aos-mirror="true"
                        data-aos-once="false"
                        data-aos-anchor-placement="top-center">
                        <i class="fas fa-check-circle"></i>
                        <p class="p-size-xs p-t-5">Success</p>
                    </div>`);
                    $(contactForm).find(".success__check").fadeIn();

                }).fail((response) => {
                    $("#formMessage").html(`
                        <h3 class="bold-6">${response.text}</h3>
                    `);
                    $(".overlay__top").addClass("error");
                    $(".overlay .trigger__close").addClass("success");
                });

            }
        });
    }
}

export default ContactForm;