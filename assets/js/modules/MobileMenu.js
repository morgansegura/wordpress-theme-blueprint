
class MobileMenu {

    constructor() {
        this.mobileMenuTrigger = $(".trigger__mobileMenu");
        this.wrapperMain = $("#mainWrapper");
        this.headerNav = $(".header__nav ul");
        this.events();
    }

    events() {
        this.mobileMenuTrigger.on("click", this.toggleMobileMenu.bind(this))
    }

    toggleMobileMenu() {
        this.mobileMenuTrigger.toggleClass("is--active");
        this.wrapperMain.toggleClass("is--active");
        this.headerNav.toggleClass("is--active");
    }
}

export default MobileMenu;