/* Our modules / classes */
import Rogue from './modules/Rogue'
import Search from './modules/Search'
import ContactForm from './modules/ContactForm'
import MobileMenu from './modules/MobileMenu'
import Hero from './modules/Hero'
import IsotopeGrid from './modules/IsotopeGrid'


/* Instantiate a new object using our modules/classes */
const rogue = new Rogue()
const search = new Search()
const contactForm = new ContactForm()
const mobileMenu = new MobileMenu()
const hero = new Hero()
const isotopeGrid = new IsotopeGrid()