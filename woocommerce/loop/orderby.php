<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<?php /*
<form class="woocommerce-ordering" method="get">
	<select name="orderby" class="orderby">
		<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
			<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
		<?php endforeach; ?>
	</select>
	<input type="hidden" name="paged" value="1" />
	<?php wc_query_string_form_fields( null, array( 'orderby', 'submit', 'paged', 'product-page' ) ); ?>
</form>

*/ ?>

	<div class="button-group">
	<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>

		<?php
		switch ( $name ) {
			case 'Sort by popularity': $name = 'Popular';
					break;
			case 'Sort by newness': $name = 'Newest';
					break;
			case 'Sort by price: low to high': $name = 'Price: Lowest';
					break;
			case 'Sort by price: high to low': $name = 'Price: Highest';
					break;
		}

		?>
		<?php //var_dump($catalog_orderby_options); ?>


		<button class="<?php echo esc_attr( $id ); ?>" data-group="<?php echo esc_attr( $id ); ?>">
			<?php echo esc_html( $name ); ?>
		</button>

		
		<?php //selected( $orderby, $id ); ?>
		<?php //echo esc_html( $name ); ?>
	<?php endforeach; ?>
	</div>
