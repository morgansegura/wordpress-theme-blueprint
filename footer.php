
            <footer class="footer">                
                <nav class="footer__nav">
                    <div class="container">
                        <ul>
                            <li class="<?php if (is_page('about-us')) echo 'current-menu-item' ?>">
                                <a href="<?php echo esc_url(site_url('/about-us')); ?>">Page Link</a>
                            </li>
                            <li class="<?php if (get_post_type() == 'program') echo 'current-menu-item' ?>">
                                <a href="<?php echo get_post_type_archive_link('/programs'); ?>">Custom Post Type Link</a>
                            </li>
                            <li class="<?php if (get_post_type() == 'program' || is_page('about-us')) echo 'current-menu-item' ?>">
                                <a href="<?php echo get_post_type_archive_link('/programs'); ?>">Custom Post Type or Page Link</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="footer__copyright">
                    <p class="container">
                        <i class="fas fa-copyright"></i> <?php the_time('Y'); ?> <?php echo bloginfo('name'); ?>. All rights reserved.
                    </p>
                </div>            
                <div class="sllc__ad">
                    <div class="container">
                        <a href="https://segurawebdesign.com"> 
                            <span class="brand">Segura</span> <span class="llc">LLC</span>
                        </a>
                    </div>
                </div>
            </footer>
        </div> <!-- [#mainWrapper] -->
        <?php wp_footer(); ?>
    </body>
</html>