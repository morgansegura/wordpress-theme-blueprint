<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset') ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php wp_head(); ?>
        <style id="outlineADA"></style>
    </head>

    <body <?php body_class(); ?>>

    <?php get_template_part('template-parts/loader') ?>

    <div id="mainWrapper" class="wrapper load-item"> 

        <header class="header">
            
            <nav class="header__nav--top">  
                <div class="container">              
                    <ul>
                        <li <?php if (get_post_type() == 'program' || is_page('about-us')) echo 'class="current-menu-item"' ?>>
                            <a href="<?php echo esc_url(site_url('/my-account')); ?>"><i class="fas fa-sign-in-alt"></i></a>
                        </li>
                        <li <?php if (get_post_type() == 'program' || is_page('about-us')) echo 'class="current-menu-item"' ?>>
                            <a href="<?php echo esc_url(site_url('/my-account/customer-logout')); ?>"><i class="fas fa-sign-out-alt"></i></a>
                        </li>
                        <li <?php if (get_post_type() == 'program' || is_page('about-us')) echo 'class="current-menu-item"' ?>>
                            <a href="<?php echo esc_url(site_url('/my-account')); ?>"><i class="fas fa-user-circle"></i></a>
                        </li>
                        <li <?php if (get_post_type() == 'program' || is_page('about-us')) echo 'class="current-menu-item"' ?>>
                            <a href="<?php echo esc_url(site_url('/cart')); ?>"><i class="fas fa-shopping-cart"></i></a>
                        </li>                    
                    </ul>
                </div>
            </nav>                
            <nav class="header__nav">  
                <div class="container">
                    <div class="flex justify-between">
                        <h3 class="logo">
                            <a href="<?php echo esc_url(site_url('/')); ?>">
                                Logo
                            </a>
                        </h3>
                        <ul>
                            <li <?php if (is_page('about-us')) echo 'class="current-menu-item"' ?>>
                                <a href="<?php echo esc_url(site_url('/about-us')); ?>">Page Link</a>
                            </li>
                            <li <?php if (get_post_type() == 'program') echo 'class="current-menu-item"' ?>>
                                <a href="<?php echo get_post_type_archive_link('/programs'); ?>">Custom Post Type Link</a>
                            </li>
                            <li <?php if (is_page('shop')) echo 'class="current-menu-item"' ?>>
                                <a href="<?php echo esc_url(site_url('/shop')); ?>">Shop</a>
                            </li>
                            <li>
                                <a class="trigger__contact" href="#">
                                Contact Us
                                </a>
                            </li>                        
                            <li>
                                <a class="trigger__search" href="#">
                                    <i class="fas fa-search"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- [Mobile Menu Trigger] -->
                        <button class="trigger__mobileMenu hamburger" type="button">
                            <span class="hamburger__box">
                                <span class="hamburger__inner"></span>
                            </span>
                        </button>          
                    </div>
                </div>
            </nav>
        </header>