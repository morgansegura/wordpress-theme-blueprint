<?php get_header(); ?>
    
    <main class="main-section load-item">

        <!-- [Grid] -->
        <section class="container p-20">
            <h3 class="t-size-xl bold-6 m-b-8">Responsive Grid</h3>
            <h4 class="t-size-lg bold-6">Two Grid Styles</h4>
            <p class="p-b-8">
                There are two ideas to think of when using this responsive grid,
                both of which are based on a common <b>12 columns</b> standard that can easily be changed in the 
                stylesheet located at: <code><b>assets/css/base/grid.css</b></code>
            </p>
            <p>
                The basic grid 
            </p>
            
            <div class="p-tb-20">
                <h5 class="t-size-md bold-6">Basic Grid</h5>
                <ul class="list disc">
                    <li>
                        <p>The grid pattern is responsive but does not change with breakpoints.</p>
                    </li>
                    <li>
                        <p>The grid pattern is responsive but does not change with breakpoints.</p>
                    </li>
                    <li>
                        <p></p>
                    </li>
                </ul>
            </div>
            <!-- 12 by 1s -->
            <p>12 individual columns</p>
            <div class="container">
                <div class="grid grid-label gap-sm-10 m-tb-10 c-red t-center t-size-xs">
                    <div class="box-1">
                        <div class="p-10 bg-red-300 box-label"></div>
                    </div>
                    <div class="box-1">
                        <div class="p-10 bg-red-300 box-label"></div>
                    </div>
                    <div class="box-1">
                        <div class="p-10 bg-red-300 box-label"></div>
                    </div>
                    <div class="box-1">
                        <div class="p-10 bg-red-300 box-label"></div>
                    </div>
                    <div class="box-1">
                        <div class="p-10 bg-red-300 box-label"></div>
                    </div>
                    <div class="box-1">
                        <div class="p-10 bg-red-300 box-label"></div>
                    </div>
                    <div class="box-1">
                        <div class="p-10 bg-red-300 box-label"></div>
                    </div>
                    <div class="box-1">
                        <div class="p-10 bg-red-300 box-label"></div>
                    </div>
                    <div class="box-1">
                        <div class="p-10 bg-red-300 box-label"></div>
                    </div>
                    <div class="box-1">
                        <div class="p-10 bg-red-300 box-label"></div>
                    </div>
                    <div class="box-1">
                        <div class="p-10 bg-red-300 box-label"></div>
                    </div>
                    <div class="box-1">
                        <div class="p-10 bg-red-300 box-label"></div>
                    </div>
                </div>
                <div class="grid gap-10 m-b-10 c-pink t-center t-size-xs">
                    <div class="box-2">
                        <div class="p-10 bg-pink-300">2</div>
                    </div>
                    <div class="box-2">
                        <div class="p-10 bg-pink-300">2</div>
                    </div>
                    <div class="box-2">
                        <div class="p-10 bg-pink-300">2</div>
                    </div>
                    <div class="box-2">
                        <div class="p-10 bg-pink-300">2</div>
                    </div>
                    <div class="box-2">
                        <div class="p-10 bg-pink-300">2</div>
                    </div>
                    <div class="box-2">
                        <p class="p-10 bg-pink-300">2</p>
                    </div>
                </div>
                <div class="grid gap-10 m-b-10 c-purple t-center t-size-xs">
                    <div class="box-3">
                        <div class="bg-purple-300 p-10">3</div>
                    </div>
                    <div class="box-3">
                        <div class="bg-purple-300 p-10">3</div>
                    </div>
                    <div class="box-3">
                        <div class="bg-purple-300 p-10">3</div>
                    </div>
                    <div class="box-3">
                        <div class="bg-purple-300 p-10">3</div>
                    </div>
                </div>
                <div class="grid gap-10 m-b-10 c-purple t-center t-size-xs">
                    <div class="box-4">
                        <div class="bg-deep-purple-300 p-10">4</div>
                    </div>
                    <div class="box-4">
                        <div class="bg-deep-purple-300 p-10">4</div>
                    </div>
                    <div class="box-4">
                        <div class="bg-deep-purple-300 p-10">4</div>
                    </div>
                </div>
                <div class="grid gap-10 m-b-10 c-indigo t-center t-size-xs">
                    <div class="box-5">
                        <div class="bg-indigo-300 p-10">5</div>
                    </div>
                    <div class="box-5">
                        <div class="bg-indigo-300 p-10">5</div>
                    </div>
                    <div class="box-2">
                        <div class="bg-indigo-300 p-10">2</div>
                    </div>
                </div>
                <div class="grid gap-10 m-b-10 c-blue t-center t-size-xs">
                    <div class="box-6">
                        <div class="bg-blue-400 p-10">6</div>
                    </div>
                    <div class="box-6">
                        <div class="bg-blue-400 p-10">6</div>
                    </div>
                </div>
                <div class="grid gap-10 m-b-10 c-light-blue t-center t-size-xs">
                    <div class="box-7">
                        <div class="bg-light-blue-300 p-10">7</div>
                    </div>
                    <div class="box-5">
                        <div class="bg-light-blue-300 p-10">5</div>
                    </div>
                </div>
                <div class="grid gap-10 m-b-10 c-cyan t-center t-size-xs">
                    <div class="box-8">
                        <div class="bg-cyan-400 p-10">8</div>
                    </div>
                    <div class="box-4">
                        <div class="bg-cyan-400 p-10">4</div>
                    </div>
                </div>
                <div class="grid gap-10 m-b-10 c-teal t-center t-size-xs">
                    <div class="box-9">
                        <div class="bg-teal-300 p-10">9</div>
                    </div>
                    <div class="box-3">
                        <div class="bg-teal-300 p-10">3</div>
                    </div>
                </div>
                <div class="grid gap-10 m-b-10 c-green t-center t-size-xs">
                    <div class="box-10">
                        <div class="bg-green-300 p-10">10</div>
                    </div>
                    <div class="box-2">
                        <div class="bg-green-300 p-10">2</div>
                    </div>
                </div>
                <div class="grid gap-10 m-b-10 c-light-green t-center t-size-xs">
                    <div class="box-11">
                        <div class="bg-light-green-400 p-10">11</div>
                    </div>
                    <div class="box-1">
                        <div class="bg-light-green-400 p-10 box-label"></div>
                    </div>
                </div>
            
                <p>And any other multiple variation of 12 you can think of.</p>

                <h5 class="p-t-20 p-b-10 bold-6 t-size-lg">What happens if the grid is uneven?</h5>
                <div class="grid gap-10 m-b-10 c-light-green t-center t-size-xs">
                    <div class="box-5">
                        <div class="bg-light-green-400 p-10">5</div>
                    </div>

                    <div class="box-6">
                        <div class="bg-light-green-400 p-10">6</div>
                    </div>
                </div>

                <div class="grid gap-10 m-b-10 c-light-green t-center t-size-xs">
                    <div class="box-5">
                        <div class="bg-light-green-400 p-10">5</div>
                    </div>
                    <div class="box-1">
                        <div class="bg-light-green-400 p-10 box-label"></div>
                    </div>
                    <div class="box-6">
                        <div class="bg-light-green-400 p-10">6</div>
                    </div>
                </div>

                <h5 class="p-t-20 p-b-10 bold-6 t-size-lg">Responsive and Media Queries</h5>

                <div class="grid-label grid gap-10 m-b-10 c-orange t-center t-size-xs">
                    <div class="box-xs-12 box-sm-6 box-md-8 box-lg-3 box-xl-9 m-b-10">
                        <div class="bg-orange-A1 p-10 box-label"></div>
                    </div>
                    <div class="box-xs-12 box-sm-6 box-md-4 box-lg-3 box-xl-3 m-b-10">
                        <div class="bg-orange-A2 p-10 box-label"></div>
                    </div>
                    <div class="box-xs-12 box-sm-6 box-md-4 box-lg-3 box-xl-3 m-b-10">
                        <div class="bg-orange-A4 p-10 box-label"></div>
                    </div>
                    <div class="box-xs-12 box-sm-6 box-md-8 box-lg-3 box-xl-9 m-b-10">
                        <div class="bg-orange-A7 p-10 box-label"></div>
                    </div>
                </div>

            </div>           
        </section>
    </main>
<?php get_footer(); ?>