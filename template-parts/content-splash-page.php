<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset') ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php wp_head(); ?>
        <style id="outlineADA"></style>
    </head>

    <body <?php body_class(); ?>>

        <div id="mainWrapper" class="wrapper load-item">     
            <header class="header"></header>
            <main class="main-section gradient">
                <section class="hero-header hero-small color_overlay gradient" data-section-type="gradient">
                    <div class="coming-soon">
                        <div class="coming-soon__message">
                            <h1><?php echo bloginfo('name'); ?> <span class="com">.com</span></h1>
                            <p class="t-size-lg p-tb-10">Our website is currently under construction</p>
                            <a class="btn btn--lg bg-white c-gray-600" href="mailto:<?php echo bloginfo('admin_email'); ?>"><?php echo bloginfo('admin_email'); ?></a>
                        </div>
                    </div>
                </section>            
            </main>
            <footer class="footer">
                <div class="container">

                    <div class="footer__copyright">
                        <p>
                            <i class="fas fa-copyright"></i> <?php the_time('Y'); ?> <?php echo bloginfo('name'); ?>. All rights reserved.
                        </p>
                    </div>
                </div>
                <div class="sllc__ad">
                    <div class="container">
                        <a href="https://segurawebdesign.com"> 
                            <span class="brand">Segura</span> <span class="llc">LLC</span>
                        </a>
                    </div>
                </div>
            </footer>
        </div> <!-- [#mainWrapper] -->

        <?php get_template_part('template-parts/loader') ?>

        <?php wp_footer(); ?>
    </body>
</html>

