<?php

require get_theme_file_path('/inc/search-route.php');


// Import external files
function theme_files() {
    /* CSS */
    wp_enqueue_script('googleMap', '//maps.googleapis.com/maps/api/js?key=AIzaSyDMBLNGf1AI9U_Ul4-ZTRXaBsJ-0wnmof0', NULL, 1.0, true);
    wp_enqueue_style('google-fonts', '//fonts.googleapis.com/css?family=Montserrat:400,600,700,800,900|Poppins:400,600,700,800,900|Norican');
    wp_enqueue_style('theme_main_styles', get_stylesheet_uri(), NULL, microtime() );
    /* JS */
    
    wp_enqueue_script('font-awesome-js', '//use.fontawesome.com/releases/v5.0.13/js/all.js', NULL, microtime(), true);
    /* Try to get these in NPM with import functions */
    wp_enqueue_script('main-theme-js', get_theme_file_uri('assets/js/scripts-bundled.js'), NULL, microtime(), true);
    /* Localize scripts */
    wp_localize_script('main-theme-js', 'themeData', array(
        'root_url' => get_site_url(),
        'theme_url' => get_theme_file_uri(),
        'mail_url' => get_theme_file_uri( '/inc/contact-form.php' )
    ));
}

add_action('wp_enqueue_scripts', 'theme_files');

// Theme function hooks
function theme_features() {
    /*
    register_nav_menu('headerMenuLocation', 'Header Menu Location');
    register_nav_menu('footerLocationOne', 'Footer Location One');
    register_nav_menu('footerLocationTwo', 'Footer Location Two');
    */
    add_theme_support('title-tag');
    add_theme_support('post-thumbnail');

    // Custom Image Sizes
    add_image_size('main-horz-huge', 1920, 1280, true);
    add_image_size('main-horz-xl', 1400, 933, true);
    add_image_size('main-horz-lg', 1280, 853, true);
    add_image_size('main-horz-md', 960, 640, true);
    add_image_size('main-horz-sm', 768, 512, true);
    add_image_size('main-horz-xs', 530, 353, true);
}

add_action('after_setup_theme', 'theme_features');

// Responsive images function
function theme_responsive_image($image_id,$image_size,$max_width){

	// check the image ID is not blank
	if($image_id != '') {

		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

		// generate the markup for the responsive image
		echo 'src="'.$image_src.'" srcset="'.$image_srcset.'" sizes="(max-width: '.$max_width.') 100vw, '.$max_width.'"';
	}
}

// Reset max image sizes
add_filter( 'max_srcset_image_width', 'theme_max_srcset_image_width', 10 , 2 );

function theme_max_srcset_image_width() {
	return 1920;
}

// Tweak WP queries 
function theme_adjust_queries($query) {
    // Events
    if (!is_admin() && is_post_type_archive('event') && $query->is_main_query()) {
        $today = date('Ymd');
        $query->set('meta_key', 'event_date');
        $query->set('orderby', 'title');
        $query->set('order', 'ASC');
        $query->set('meta_query', array(
            array(
              'key' => 'event_date',
              'compare' => '>=',
              'value' => $today,
              'type' => 'numeric'
            )
          ));
    }
    // Programs
    if (!is_admin() && is_post_type_archive('program') && $query->is_main_query()) {
        $query->set('orderby', 'title');
        $query->set('order', 'ASC');
        $query->set('posts_per_page', -1);
    }

    // Campuses
    if (!is_admin() && is_post_type_archive('campus') && $query->is_main_query()) {
        $query->set('posts_per_page', -1);
    }
}

add_action('pre_get_posts', 'theme_adjust_queries');

// ACF Google Map API Key
function themeMapKey($api) {
    $api['key'] = 'AIzaSyDMBLNGf1AI9U_Ul4-ZTRXaBsJ-0wnmof0';
    return $api;
}
add_filter('acf/fields/google_map/api', 'themeMapKey');


/* Theme Dev Tools */
function print_pre($args) {
  echo '<pre>';
  print_r($arg);
  echo '</pre>';
}

/* Woo Commerce */


add_action( 'wp_enqueue_scripts', 'child_manage_woocommerce_styles', 99 );

function child_manage_woocommerce_styles() {
    //remove generator meta tag
    remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );

    //first check that woo exists to prevent fatal errors
    if ( function_exists( 'is_woocommerce' ) ) {
        //dequeue scripts and styles
        if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
            wp_dequeue_style( 'woocommerce_frontend_styles' );
            wp_dequeue_style( 'woocommerce_fancybox_styles' );
            wp_dequeue_style( 'woocommerce_chosen_styles' );
            wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
            wp_dequeue_script( 'wc_price_slider' );
            wp_dequeue_script( 'wc-single-product' );
            wp_dequeue_script( 'wc-add-to-cart' );
            wp_dequeue_script( 'wc-cart-fragments' );
            wp_dequeue_script( 'wc-checkout' );
            wp_dequeue_script( 'wc-add-to-cart-variation' );
            wp_dequeue_script( 'wc-single-product' );
            wp_dequeue_script( 'wc-cart' );
            wp_dequeue_script( 'wc-chosen' );
            wp_dequeue_script( 'woocommerce' );
            wp_dequeue_script( 'prettyPhoto' );
            wp_dequeue_script( 'prettyPhoto-init' );
            wp_dequeue_script( 'jquery-blockui' );
            wp_dequeue_script( 'jquery-placeholder' );
            wp_dequeue_script( 'fancybox' );
            wp_dequeue_script( 'jqueryui' );
        }
    }
 }
/* Detect template file names*/
add_action( 'wp_head', 'showWPGlobal', 999 );

function showWPGlobal() {
    /* https://codex.wordpress.org/Global_Variables  */
    // global $wp;
    // global $wp; // Public Query Vars
    // global $wp_locale; // Date/Time
    // global $wpdb; // Global DB
    //global $pagenow ; // Global DB
    //var_dump($pagenow);
}

function debug($string1, $string2) {

    echo '<div class="debug template-finder">';
    echo '<div class="flex justify-between items-center template-finder__name">';

    if ($string1 != null) {
    echo '<p class="bg-green-500 p-5 m-r-5"> '. $string1 .' </p>';
    }
    if ($string2 != null) {
    echo '<p class="bg-green-800 p-5"> '. $string2 .' </p>';
    }
    echo '</div>';
    echo '</div>';    
}