<form class="search__form" method="get" action="<?php echo esc_url(site_url('/')); ?>">
    <label class="headline headline--medium" for="s">Perform a New Search</label>
    <div>
        <input placeholder="What are you looking for?" id="s" class="s" type="search" name="s">
        <input class="search-submit" type="submit" value="Search" >
    </div>
</form>