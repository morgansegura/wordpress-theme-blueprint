<?php
if($_POST) {

	$to_email = 'morgansegura@gmail.com'; //Change to your e-mail
	$from_email = 'noreply@morgansegura.me'; //Change to your e-mail

	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
		
		$output = json_encode(array( //create JSON data
			'type'=>'error', 
			'text' => 'Sorry Request must be Ajax POST'
		));
		die($output); //exit script outputting json data
	} 

	$name = $_POST['name'];
	$subject = $_POST['subject'];
	$email = $_POST['email'];
	$message = $_POST['message'];

	$name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
	$subject = filter_var($_POST['subject'], FILTER_SANITIZE_STRING);
	$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
	$message = filter_var($_POST['message'], FILTER_SANITIZE_STRING);

	if (strlen($name) < 4) {
		$output = json_encode(
			array(
				'type'=>'error', 
				'text' => 'Name is too short or empty!'
			)
		);
		die($output);
	}

	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$output = json_encode(
			array(
				'type'=>'error', 
				'text' => 'Please enter a valid email!'
			)
		);
		die($output);
	}

	if(strlen($subject) < 3){ //check emtpy subject
		$output = json_encode(
			array(
				'type'=>'error', 
				'text' => 'Subject is required'
			)
		);
		die($output);
	}

	if(strlen($message)<3){ //check emtpy message
		$output = json_encode(
			array(
				'type'=>'error', 
				'text' => 'Too short message! Please enter something.'
			)
		);
		die($output);
	}

	//email body
    $message_body = $message."\r\n\r\n-".$name."\r\nEmail : ".$email;

    //proceed with PHP email.
    $headers = 'From: '. $from_email .'' . "\r\n" .
    'Reply-To: '.$email.'' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

	$send_mail = mail($to_email, $subject, $message_body, $headers);

    if(!$send_mail)
    {
        //If mail couldn't be sent output error. Check your PHP email configuration (if it ever happens)
		$output = json_encode(array('type'=>'error', 'text' => '<h3 class="message__headline">Uh oh.</h3>'. "\r\n" . '<p class="message__response">Could not send mail! Please check your PHP mail configuration.</p>'));
        die($output);
    }else{
        $output = json_encode(array('type'=>'message', 'text' => '<h3 class="message__headline">Thank you '.$name .'.</h3>'. "\r\n" . '<p class="message__response">I will get back to in a day or so.</p>'));
        die($output);
	}
	

}

